import os
from flask import Flask, send_from_directory
from flask_restful import Api
from flask_swagger_ui import get_swaggerui_blueprint
from catalog_models.models import db
from resources.task_resources import TaskResource
from config import Config

app = Flask(__name__)
app.config.from_object(Config)

# Инициализация базы данных с приложением
db.init_app(app)

# Настройка API с помощью Flask-RESTful
api = Api(app)


@app.before_request
def before_request():
    """
    Создание всех таблиц базы данных перед каждым запросом, если они не существуют.
    """
    db.create_all()


# Определение маршрутов для ресурсов Task
api.add_resource(TaskResource, '/tasks', '/tasks/<int:task_id>')

# Конфигурация Swagger UI
SWAGGER_URL = '/swagger'
API_URL = '/swagger/swagger.yaml'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "TODO API"
    }
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


@app.route('/swagger/swagger.yaml')
def swagger_yaml():
    """
    Обслуживание файла swagger.yaml.
    """
    return send_from_directory(os.path.join(os.path.dirname(__file__), 'swagger'), 'swagger.yaml')


if __name__ == '__main__':
    app.run(debug=True)
