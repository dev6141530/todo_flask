from flask import request
from flask_restful import Resource
from marshmallow import ValidationError
from catalog_models.models import db, Task
from schemas.schemas import task_schema, tasks_schema


class TaskResource(Resource):
    def get(self, task_id=None):
        """
        Обработка GET-запросов.
        """
        try:
            if task_id:
                task = Task.query.get(task_id)
                if not task:
                    return {'message': 'Task not found'}, 404
                return task_schema.dump(task)
            tasks = Task.query.all()
            return tasks_schema.dump(tasks)
        except ValidationError as err:
            return err.messages, 400
        except Exception as err:
            return {'message': str(err)}, 500

    def post(self):
        """
        Обработка POST-запросов.
        """
        try:
            data = task_schema.load(request.get_json())
            new_task = Task(**data)
            db.session.add(new_task)
            db.session.commit()
            return task_schema.dump(new_task), 201
        except ValidationError as err:
            return err.messages, 400
        except Exception as err:
            return {'message': str(err)}, 500

    def put(self, task_id):
        """
        Обработка PUT-запросов.
        """
        try:
            task = Task.query.get(task_id)
            if not task:
                return {'message': 'Task not found'}, 404

            data = task_schema.load(request.get_json())
            for key, value in data.items():
                setattr(task, key, value)

            db.session.commit()
            return task_schema.dump(task), 200
        except ValidationError as err:
            return err.messages, 400
        except Exception as err:
            return {'message': str(err)}, 500

    def delete(self, task_id):
        """
        Обработка DELETE-запросов.
        """
        try:
            task = Task.query.get(task_id)
            if not task:
                return {'message': 'Task not found'}, 404

            db.session.delete(task)
            db.session.commit()
            return {'message': 'Task deleted successfully'}, 200
        except ValidationError as err:
            return err.messages, 400
        except Exception as err:
            return {'message': str(err)}, 500
