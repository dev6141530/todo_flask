from marshmallow import Schema, fields, validate, post_dump
from collections import OrderedDict


class TaskSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(required=True, validate=validate.Length(min=1, max=120))
    description = fields.Str(validate=validate.Length(max=250))
    created_at = fields.DateTime(dump_only=True)
    updated_at = fields.DateTime(dump_only=True)

    @post_dump
    def keep_order(self, data, **kwargs):
        """
        Порядок полей при сериализации.
        """
        return OrderedDict([
            ('id', data.get('id')),
            ('title', data.get('title')),
            ('description', data.get('description')),
            ('created_at', data.get('created_at')),
            ('updated_at', data.get('updated_at')),
        ])


task_schema = TaskSchema()
tasks_schema = TaskSchema(many=True)
