import unittest
import json
from todo_flask.app import app, db


class TaskTestCase(unittest.TestCase):

    def setUp(self):
        """
        Настройка тестового клиента и создание таблиц базы данных перед каждым тестом.
        """
        self.app = app.test_client()
        self.app.testing = True

        with app.app_context():
            db.create_all()

    def tearDown(self):
        """
        Удаление всех записей и таблиц базы данных после каждого теста.
        """
        with app.app_context():
            db.session.remove()
            db.drop_all()

    def test_create_task(self):
        """
        Тестирование создания новой задачи.
        """
        # Тест создания задачи с валидными данными
        response = self.app.post('/tasks', data=json.dumps({'title': 'Test Task'}), content_type='application/json')
        self.assertEqual(response.status_code, 201)
        self.assertIn('Test Task', response.data.decode())

        # Тест создания задачи без заголовка
        response = self.app.post('/tasks', data=json.dumps({}), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertIn('Title is required', response.data.decode())

    def test_get_tasks(self):
        """
        Тестирование получения списка всех задач.
        """
        response = self.app.get('/tasks')
        self.assertEqual(response.status_code, 200)

    def test_get_task(self):
        """
        Тестирование получения задачи по ID.
        """
        self.app.post('/tasks', data=json.dumps({'title': 'Test Task'}), content_type='application/json')
        response = self.app.get('/tasks/1')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Test Task', response.data.decode())

    def test_update_task(self):
        """
        Тестирование обновления задачи.
        """
        self.app.post('/tasks', data=json.dumps({'title': 'Test Task'}), content_type='application/json')
        response = self.app.put('/tasks/1', data=json.dumps({'title': 'Updated Task'}), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Updated Task', response.data.decode())

    def test_delete_task(self):
        """
        Тестирование удаления задачи.
        """
        self.app.post('/tasks', data=json.dumps({'title': 'Test Task'}), content_type='application/json')
        response = self.app.delete('/tasks/1')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Task deleted successfully', response.data.decode())


if __name__ == '__main__':
    unittest.main()
